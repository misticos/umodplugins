﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Oxide.Core;
using UnityEngine;
using Random = System.Random;

namespace Oxide.Plugins
{
    [Info("Plant Drop", "Iv Misticos", "1.2.4")]
    [Description("Allows planting anywhere by dropping the seed")]
    class PlantDrop : RustPlugin
    {
        #region Variables
        
        private const string PrefabCorn = "assets/prefabs/plants/corn/corn.entity.prefab";
        private const string PrefabHemp = "assets/prefabs/plants/hemp/hemp.entity.prefab";
        private const string PrefabPotato = "assets/prefabs/plants/potato/potato.entity.prefab";
        private const string PrefabPumpkin = "assets/prefabs/plants/pumpkin/pumpkin.entity.prefab";
        
        private readonly Random _random = new Random();
        
        #endregion
        
        #region Configuration

        private Configuration _config;
        
        private class Configuration
        {
            [JsonProperty(PropertyName = "Randomize Position")]
            public bool RandomizePosition = false;
            
            [JsonProperty(PropertyName = "Randomize Position On N")]
            public float RandomizePositionOn = 2f;
        }

        protected override void LoadConfig()
        {
            base.LoadConfig();
            try
            {
                _config = Config.ReadObject<Configuration>();
                if (_config == null) throw new Exception();
            }
            catch
            {
                PrintError("Your configuration file contains an error. Using default configuration values.");
                LoadDefaultConfig();
            }
        }

        protected override void LoadDefaultConfig() => _config = new Configuration();

        protected override void SaveConfig() => Config.WriteObject(_config);

        #endregion
        
        #region Hooks
        
        private void OnItemDropped(Item item, BaseNetworkable entity)
        {
            if (item?.info == null || entity == null)
                return;
            
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (item.info.shortname)
            {
                case "seed.corn":
                    CreatePlant(entity, PrefabCorn, entity.transform.position, item.amount);
                    break;

                case "seed.hemp":
                    CreatePlant(entity, PrefabHemp, entity.transform.position, item.amount);
                    break;

                case "seed.potato":
                    CreatePlant(entity, PrefabPotato, entity.transform.position, item.amount);
                    break;

                case "seed.pumpkin":
                    CreatePlant(entity, PrefabPumpkin, entity.transform.position, item.amount);
                    break;
            }
        }
        
        #endregion
        
        #region Helpers
        
        private void CreatePlant(BaseNetworkable seedItem, string prefab, Vector3 pos, int amount)
        {
            RaycastHit hit;
            Physics.Raycast(pos, Vector3.down, out hit);
            if (hit.GetEntity() != null)
                return;

            seedItem.Kill();
            ServerMgr.Instance.StartCoroutine(PlantCoroutine(prefab, pos, amount));
        }

        private static readonly WaitForFixedUpdate WaitForFixedUpdate = new WaitForFixedUpdate();
        private IEnumerator PlantCoroutine(string prefab, Vector3 origin, int amount)
        {
            var position = origin;
            for (var i = 0; i < amount; i++)
            {
                if (_config.RandomizePosition)
                {
                    position.x = origin.x + UnityEngine.Random.Range(-_config.RandomizePositionOn, _config.RandomizePositionOn);
                    position.z = origin.y + UnityEngine.Random.Range(-_config.RandomizePositionOn, _config.RandomizePositionOn);
                    position.y = TerrainMeta.HeightMap.GetHeight(position);
                }
                
                var plant = GameManager.server.CreateEntity(prefab, position, Quaternion.identity) as GrowableEntity;
                if (plant == null)
                    continue;

                plant.Spawn();
                yield return WaitForFixedUpdate;
            }
        }
        
        #endregion
    }
}