using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Oxide.Core.Libraries.Covalence;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("Skin It", "misticos", "1.0.0")]
    [Description("Skin entities by looking at them")]
    class SkinIt : CovalencePlugin
    {
        #region Variables
        
        private const string PermissionUse = "skinit.use";
        
        #endregion
        
        #region Configuration

        private Configuration _config;

        private class Configuration
        {
            [JsonProperty(PropertyName = "Commands")]
            public string[] Commands = {"skinit"};
        }

        protected override void LoadConfig()
        {
            base.LoadConfig();
            try
            {
                _config = Config.ReadObject<Configuration>();
                if (_config == null) throw new Exception();
                SaveConfig();
            }
            catch
            {
                PrintError("Your configuration file contains an error. Using default configuration values.");
                LoadDefaultConfig();
            }
        }

        protected override void SaveConfig() => Config.WriteObject(_config);

        protected override void LoadDefaultConfig() => _config = new Configuration();

        #endregion
        
        #region Hooks

        protected override void LoadDefaultMessages()
        {
            lang.RegisterMessages(new Dictionary<string, string>
            {
                {"Player Only", "This command is only for players"},
                {"Invalid Skin", "Please, provide a valid skin"},
                {"No Permission", "You do not have enough permissions"},
                {"No Entity", "Please, look at a valid entity"},
                {"Done", "Skin was applied"}
            }, this);
        }

        private void Init()
        {
            permission.RegisterPermission(PermissionUse, this);
            
            AddCovalenceCommand(_config.Commands, nameof(CommandSkinIt));
        }
        
        #endregion
        
        #region Commands

        private void CommandSkinIt(IPlayer player, string command, string[] args)
        {
            var basePlayer = player.Object as BasePlayer;
            if (basePlayer == null)
            {
                player.Reply(GetMsg("Player Only", player.Id));
                return;
            }

            ulong skin;
            if (args.Length != 1 || !ulong.TryParse(args[0], out skin))
            {
                player.Reply(GetMsg("Invalid Skin", player.Id));
                return;
            }

            if (!player.HasPermission(PermissionUse))
            {
                player.Reply(GetMsg("No Permission", player.Id));
                return;
            }

            RaycastHit hit;
            if (!Physics.Raycast(basePlayer.eyes.HeadRay(), out hit))
            {
                player.Reply(GetMsg("No Entity", player.Id));
                return;
            }

            var entity = hit.GetEntity();
            if (entity == null)
            {
                player.Reply(GetMsg("No Entity", player.Id));
                return;
            }

            entity.skinID = skin;
            entity.SendNetworkUpdateImmediate();
            
            player.Reply(GetMsg("Done", player.Id));
        }
        
        #endregion
        
        #region Helpers

        private string GetMsg(string key, string userId = null) => lang.GetMessage(key, this, userId);

        #endregion
    }
}